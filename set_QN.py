# Simple script to set QN and QN of gradients for 4 species cases. To be generalised in future

class Species:  
  def __init__(self):
    self.omn  = []
    self.dens = []
    self.charge = []

  
  def set_qn(self, num_spec, index1=1, index2=1):  
    output = []
    sum1 = 0
    sum2 = 0
    for i in range(0,num_spec):       
      if i != index1:
        sum1 += self.charge[i]*self.dens[i]
    self.dens[index1] = -sum1 / self.charge[index1]
    for i in range(0,num_spec):         
      if i != index2:
        sum2 += self.charge[i]*self.dens[i]*self.omn[i]     
    self.omn[index2] = -sum2/(self.charge[index2]*self.dens[index2])
        
    return (self.dens[index1], self.omn[index2])

if __name__ == '__main__':

  spec = Species()

  spec.omn.append(0.2903) # electrons
  spec.omn.append(0.2903) # main ion
  spec.omn.append(3.733596562536291) # imp 1
  spec.omn.append(-0.5022645353489722) # imp 2 
  
  spec.charge.append(-1) # electrons
  spec.charge.append(1) # main ion
  spec.charge.append(1) # imp 1
  spec.charge.append(17) # imp 2

  spec.dens.append(1.0) # electrons
  spec.dens.append(0.9158) # main ion
  spec.dens.append(0.0434) # imp 1
  spec.dens.append(0.0024) # imp 2
  
  # num species, QN, QN of gradients
  print(spec.set_qn(4,1,1))

