# usage example: python plot_lin.py 168,0,169,0 --semilogx
# argparse reads comma separated values corresponding to pairs 
# of run directory # and scanfiles directories
# --semilogx is option for logarithmic x-axis

# plots gam and omega for scans in rundirectories $WORK/linmar168/scanfiles0000, $WORK/linmar169/scanfiles0000

# implicitly assumes run directories with naming convention linmar###
# can/should be generalised

# if not on Marconi, ensure that $WORK is defined and points to where your run directories are

import sys
import os
import matplotlib.pyplot as plt
import numpy as np
import glob
import argparse
from IPython import embed # used for debugging only

class Spec():
  def __init__(self):
    self.scanval = []
    self.kymin = [] 
    self.n = []
    self.gam = []
    self.ome = []

  # read gamma, omega, scan variable name, scan variable value
  def parse_scanlog(self,file_location):
    with open(file_location+'/scan.log','r') as scanlog:
      for i,line in enumerate(scanlog):
        if i > 0:
          self.scanval.append(float(line.split()[2]))
          self.gam.append(float(line.split()[4]))
          self.ome.append(float(line.split()[5]))
        else:
          self.scanname = line.split()[2]
   
  # read n0_global, kymin, and Lref
  def parse_parameters(self,file_location):
    parfiles = glob.glob(rundir+'/parameters_*')
    parfiles.sort()
    for i,parfile in enumerate(parfiles):
      with open(parfile,'r') as pfile:
        for line in pfile:
          if len(line.split()) > 0:
            if line.split()[0] == 'kymin':
              self.kymin.append(float(line.split()[2]))
            if line.split()[0] == 'n0_global':
              self.n.append(int(line.split()[2]))
            if line.split()[0] == 'Lref' and i == 0:
              self.Lref = float(line.split()[2])

  # filter out nans from dataset
  def nan_filter(self):
    finite_mask = np.isfinite(self.gam)
    
    def nan_filter_list(list_in):
      list_out = []
      for el, mask in zip(list_in, finite_mask):
         if mask:
           list_out.append(el)
      return list_out

    for name in ['gam','ome','kymin','n','scanval']:
      setattr(self, name, nan_filter_list(getattr(self, name)))
     
rundirs = []
runnames = []
specs = []

# read comma separated inputs from CLI
parser = argparse.ArgumentParser()
parser.add_argument('fileinfo')
parser.add_argument('-slx', '--semilogx', action='store_true', 
    help='sets x-axis to semilogx')

args = parser.parse_args()
fileinfo = args.fileinfo.split(',')

# Set path to run directories based on user input
for i in range(len(fileinfo)):
  if ~i%2: 
    runname = 'linmar'+fileinfo[i]
    runnames.append(runname)
    rundirs.append(os.environ['WORK']+'/'+runname+'/scanfiles000'+fileinfo[i+1])

# Instantiate spectrum classes in list, orase scan.log and parameters and read values
for i,rundir in enumerate(rundirs):
  specs.append(Spec()) 
  specs[i].parse_scanlog(rundir)
  specs[i].parse_parameters(rundir)
  specs[i].nan_filter()

# Sanity check that all scans are over same variable
scannames = []
for spec in specs:
  scannames.append(spec.scanname)
if len(set(scannames)) > 1:
  raise ValueError('Scans of chosen runs are over different variables. Fail!')

# Renormalize all Lref to Lref of the first scan. Can be generalized later
for i,spec in enumerate(specs):
  if i > 0:
    spec.gam = list(map(lambda x: x*specs[0].Lref/spec.Lref, spec.gam))
    spec.ome = list(map(lambda x: x*specs[0].Lref/spec.Lref, spec.ome))

# Plot!

plt.figure()
plt.subplot(121)
for spec in specs:
  if spec.scanname == 'kymin':
    if args.semilogx:
      plt.semilogx(spec.n, spec.gam)
    else:
      plt.plot(spec.n, spec.gam)
    plt.xlabel('$n_{tor}$')
  else:
    if args.semilogx:
      plt.semilogx(spec.scanval, spec.gam)
    else:
      plt.plot(spec.scanval, spec.gam)
    plt.xlabel(spec.scanname)
plt.ylabel('$\gamma~[c_s/L_{ref}]$'+'. $L_{ref}$ = '+str(round(specs[0].Lref,3)))

plt.subplot(122)
for spec in specs:
  if spec.scanname == 'kymin':
    if args.semilogx:
      plt.semilogx(spec.n, spec.ome)
    else:
      plt.plot(spec.n, spec.ome)
    plt.xlabel('$n_{tor}$')
  else:
    if args.semilogx:
      plt.semilogx(spec.scanval, spec.ome)
    else:
      plt.plot(spec.scanval, spec.ome)
    plt.xlabel(spec.scanname)
plt.ylabel('$\omega~[c_s/L_{ref}]$')

plt.legend(runnames)

plt.show() 
